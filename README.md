# BUT
## Bash Unit Tests library

This is a homemade bash unit testing library designed to be 
included in some CI toolchains.

It generates xUnit compatible XML results that may be used in Jenkins dashboards.

